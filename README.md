# Execução
O programa pode ser executado através do script $Trab5mc920.py$.
O script recebe como argumento duas imagem de entrada que se encontra no mesmo diretório que o arquivo, o descritor e matcher desejado, o algoritmo irá executar todas as transformações, salvar as imagens resultantes no mesmo diretório.
Para rodar o script podemos fazer dessa forma :

``` python
python3 Trab5mc920.py nome_imagem_a nome_imagem_b nome_descritor matcher
```

# output 
O output será salvo na mesma pasta e com os seguintes nomes :

* A/B\_img.jpg : imagem cinza;
* A/B\_[nome\_descritor]\_keypoints.jpg : imagem cinza com os pontos chaves;
*  C\_[nome\_descritor]\_[matcher]\_linhas\_unidas.jpg : imagem interligando so pontos entre as duas imagens;
* AB\_[nome\_descritor]\_[matcher]\_panorama.jpg : a imagem unida;
* AB\_[nome\_descritor]\_[matcher]\_panorama\_trim.jpg : a imagem unida com um corte nos eixos.
