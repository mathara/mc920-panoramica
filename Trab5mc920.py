#!/usr/bin/python
import cv2
import sys
import numpy as np

#realiza o corte na imagem
def trim(frame):
    #crop top
    if not np.sum(frame[0]):
        return trim(frame[1:])
    #crop bottom
    elif not np.sum(frame[-1]):
        return trim(frame[:-2])
    #crop left
    elif not np.sum(frame[:,0]):
        return trim(frame[:,1:]) 
    #crop right
    elif not np.sum(frame[:,-1]):
        return trim(frame[:,:-2])    
    return frame

#Constantes
MIN_MATCH_COUNT = 4
FLANN_INDEX_KDTREE = 0


#a imagem que foi passada como parâmetros
nome_imagem_a = sys.argv[1]
nome_imagem_b = sys.argv[2]
nome_descritor = sys.argv[3]
matcher = sys.argv[4]

# criando a imagem,convertendo para a escala de cinza
img_a = cv2.imread(nome_imagem_a)
img_b = cv2.imread(nome_imagem_b)

#convertendo para a escala de cinza
img_a_gray = cv2.cvtColor(img_a, cv2.COLOR_BGR2GRAY)
img_b_gray = cv2.cvtColor(img_b, cv2.COLOR_BGR2GRAY)

#salvar imagem cinza
cv2.imwrite("A_img.jpg", img_a_gray)
cv2.imwrite("B_img.jpg", img_b_gray)


#inicializando detector SIFT, SURF
if (nome_descritor == 'SIFT'):
    descritor = sift = cv2.xfeatures2d.SIFT_create()
elif (nome_descritor == 'SURF'):
    descritor = surf = cv2.xfeatures2d.SURF_create()
elif (nome_descritor == 'ORB'):
    descritor = orb = cv2.ORB_create(nfeatures=1500)
elif (nome_descritor == 'BRISK'):
    descritor = brisk = cv2.BRISK_create()

#pontos chaves e descritores , imagem A
kp_a,des_a = descritor.detectAndCompute(img_a_gray,None)
img_a_gray = cv2.drawKeypoints(img_a_gray,kp_a,None,color=(0,255,0))
cv2.imwrite('A_'+ nome_descritor +'_keypoints.jpg',img_a_gray)

#pontos chaves e descritores , imagem B
kp_b,des_b = descritor.detectAndCompute(img_b_gray,None)
img_b_gray = cv2.drawKeypoints(img_b_gray,kp_b,None,color=(0,255,0))
cv2.imwrite('B_'+ nome_descritor +'_keypoints.jpg',img_b_gray)


if (matcher == 'FLANN'):
    #FLANN matcher code:
    FLANN_INDEX_KDTREE = 0
    index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 5)
    search_params = dict(checks = 50)

    match = cv2.FlannBasedMatcher(index_params, search_params)
    matches = match.knnMatch(np.float32(des_a),np.float32(des_b),k=2)
else:# (matcher == 'BFMatcher'):
    #BFMatcher matcher code:
    match = cv2.BFMatcher()
    matches = match.knnMatch(np.float32(des_a),np.float32(des_b),k=2)



#escolhe os melhores correspondências entre as duas imagens com o ratio de tolerância
ratio = 0.5
good = []
for m,n in matches:
    if m.distance < ratio*n.distance:
        good.append(m)

#desenhar retas entre pontos correspondentes no par de imagens.
draw_params = dict(matchColor = (0,255,0), # draw matches in green color
                   singlePointColor = None,
                   #matchesMask = matchesMask, # draw only inliers
                   flags = 2)

img_c = cv2.drawMatches(img_a_gray,kp_a,img_b_gray,kp_b,good,None,**draw_params)
cv2.imwrite('C_'+ nome_descritor +'_linhas_unidas.jpg',img_c)


if len(good)>MIN_MATCH_COUNT:
    src_pts = np.float32([ kp_a[m.queryIdx].pt for m in good ]).reshape(-1,1,2)
    dst_pts = np.float32([ kp_b[m.trainIdx].pt for m in good ]).reshape(-1,1,2)

    M, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC,5.0)
    matchesMask = mask.ravel().tolist()

    h,w,c = img_a.shape
    pts = np.float32([ [0,0],[0,h-1],[w-1,h-1],[w-1,0] ]).reshape(-1,1,2)
    dst = cv2.perspectiveTransform(pts,M)
else:
    print ("Not enough matches are found - %d/%d" % (len(good),MIN_MATCH_COUNT))
    matchesMask = None

#une as imagens
img_p = cv2.warpPerspective(img_a, M,
	(img_a.shape[1] + img_b.shape[1], img_a.shape[0]) )

img_p[0:img_b.shape[0], 0:img_b.shape[1]] = img_b

#panorama
cv2.imwrite('AB_'+ nome_descritor +"_"+ matcher +'_panorama.jpg',img_p)
#panorama com o corte 
cv2.imwrite('AB_'+ nome_descritor +"_"+ matcher +'_panorama_trim.jpg',trim(img_p))